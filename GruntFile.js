'use strict';

module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		// Read package meta data
		pkg: grunt.file.readJSON('package.json'),

		clean: {
			build: ['./build']
		},

		copy: {
			build: {
				files: [
					{
						expand: true,
						src: ['./*.php', 'js/*', 'css/*', 'images/*'],
						dest: './build/'
					}
				]
			}
		},

		compress: {
			build: {
				options: {
					archive: 'builds/<%= pkg.name %>-<%= pkg.version %>.zip',
					level: 9
				},
				expand: true,
				cwd: 'build/',
				src: ['**/*'],
				dest: '<%= pkg.name %>/'
			}
		}
	});

	grunt.registerTask('default', [
		'clean:build',
		'copy:build',
		'compress:build',
		'clean:build',
	]);
}
