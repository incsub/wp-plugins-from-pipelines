(function ($) {
	function tokens_available() {
		return (pfp_vars || {}).tokens_available;
	}

	function init() {
		if (tokens_available()) {
			post('pfp_refresh_access_token').done(function (response) {
				response = response || {};
				if (response.success) {
					select_plugin_div().removeClass('pfp-loading');
				}
			});
		}

		$(document)
			.on('click', '#pfp-select-plugin-button', handle_select_plugin_click)
			.on('click', '.pfp-box-head', handle_box_head_click)
			.on('click', '#pfp-select-build-button', handle_select_build_click);
	}

	$(init);

	function handle_box_head_click() {
		$(this).closest('.pfp-box').toggleClass('pfp-box-close');
	}

	function select_plugin_div() {
		return $('#pfp-select-plugin');
	}

	function select_build_div() {
		return $('#pfp-select-build');
	}

	function handle_select_plugin_click(e) {
		e.preventDefault();
		e.stopPropagation();

		var $button = $(this),
			$select_plugin = select_plugin_div(),
			$select_build = select_build_div();

		$button.prop('disabled', true);
		$select_plugin.addClass('pfp-loading');

		post('pfp_get_download_list', {
			plugin: $('select', $select_plugin).val()
		}).done(function (response) {
			response = response || {};
			if (response.success && response.data) {
				$select_plugin.hide();

				$select_build.show();
				$('#pfp-select-build-inner').html(response.data);
			}
		});
	}

	function handle_select_build_click(e) {
		e.preventDefault();
		e.stopPropagation();

		var $button = $(this),
			$select_build = select_build_div(),
			$installed = $('#pfp-installed');

		$button.prop('disabled', true);
		$select_build.addClass('pfp-loading');

		post('pfp_install_plugin', {
			url: $('select', $select_build).val()
		}).done(function (response) {
			response = response || {};
			if (response.success) {
				$select_build.hide();
				$installed.show();
			}
		});
	}

	function post(action, data) {
		var reload = false;
		data = $.extend({
			action: action,
			_wpnonce: $('#_wpnonce').val()
		}, data);

		return $.post(ajaxurl, data)
			.fail(function () {
				reload = confirm('Error! Start over?');
			})
			.done(function (response) {
				response = response || {};
				if (!response.success) {
					if ((response.data || {}).errors) {
						reload = confirm(response.data.errors + "\n\nStart over?");
					} else {
						reload = confirm('Error! Start over?');
					}
					if (reload) {
						window.location.reload();
					} else {
						show_error_state();
					}
				}
			});
	}

	function show_error_state() {
		$('#pfp-container .pfp-box-body > div').hide();
		$('#pfp-error').show();
	}
})(jQuery);
