<?php

class PFP_Admin_Page {
	private $page_hook;
	private $plugins = array(
		'google-analytics-async'    => 'Beehive',
		'ultimate-branding'         => 'Branda',
		'broken-link-checker'       => 'Broken Link Checker',
		'wp-defender'               => 'Defender',
		'forminator'                => 'Forminator',
		'wp-hummingbird'            => 'Hummingbird',
		'hustle'                    => 'Hustle',
		'wpmudev-videos'            => 'Integrated Video Tutorials',
		'wp-plugins-from-pipelines' => 'Plugins from Pipelines',
		'wpmu-dev-seo'              => 'SmartCrawl',
		'wp-smushit'                => 'Smush',
		'snapshot-backups'          => 'Snapshot',
		'wpmudev-updates'           => 'WPMU DEV Dashboard',
	);

	function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'register_assets' ), - 10 );
		add_action( 'admin_menu', array( $this, 'add_page' ), 20 );
	}

	public function register_assets( $hook ) {
		if ( $hook !== $this->page_hook ) {
			return;
		}

		wp_register_script( 'pfp-admin-page', plugin_dir_url( __FILE__ ) . 'js/admin-page.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'pfp-admin-page', 'pfp_vars', array(
			'tokens_available' => Plugins_From_Pipelines::tokens_available(),
		) );
		wp_enqueue_style( 'pfp-admin-page-styling', plugin_dir_url( __FILE__ ) . 'css/admin-page.css', array(), '1.0.0', 'all' );
	}

	public function add_page() {
		$this->page_hook = add_menu_page(
			esc_html__( 'Plugins From Pipelines', 'pfp' ),
			esc_html__( 'Plugins From Pipelines', 'pfp' ),
			'manage_options',
			'pfp_admin_page',
			array( $this, 'admin_page' ),
			''
		);
	}

	public function admin_page() {
		wp_enqueue_script( 'pfp-admin-page' );

		$tokens_available = Plugins_From_Pipelines::tokens_available();

		?>
		<div class="wrap" id="pfp-page-wrap">
			<h1><?php esc_html_e( 'Plugins From Pipelines', 'pfp' ); ?></h1>

			<div id="pfp-container"
			     class="pfp-box"
			     style="<?php echo $tokens_available ? '' : 'display:none;'; ?>">

				<?php wp_nonce_field( 'pfp_ajax_call' ) ?>

				<div class="pfp-box-body">
					<div id="pfp-select-plugin" class="pfp-loading">
						<label for="plugin"><?php esc_attr_e( 'Select a plugin', 'pfp' ); ?></label>
						<select id="plugin">
							<?php foreach ( $this->plugins as $plugin_repo_slug => $plugin_name ): ?>
								<option value="<?php echo esc_attr( $plugin_repo_slug ); ?>">
									<?php echo esc_html( $plugin_name ); ?>
								</option>
							<?php endforeach; ?>
						</select>

						<button id="pfp-select-plugin-button" class="button button-primary">
							<?php esc_html_e( 'Go', 'pfp' ); ?>
						</button>
					</div>

					<div id="pfp-select-build" style="display: none;">
						<label for="build"><?php esc_attr_e( 'Select a version', 'pfp' ); ?></label>
						<div id="pfp-select-build-inner"></div>
						<button id="pfp-select-build-button"
						        class="button button-primary">

							<?php esc_html_e( 'Install', 'pfp' ); ?>
						</button>
						<a id="pfp-abort-select-build-button"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>"
						   class="button">

							<?php esc_html_e( 'Back', 'pfp' ); ?>
						</a>
					</div>

					<div id="pfp-installed" style="display:none;">
						<?php esc_html_e( 'Installed!', 'pfp' ); ?> &#127881;
						<a class="button button-primary"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>">
							<?php esc_html_e( 'Install Another', 'pfp' ); ?>
						</a>
					</div>

					<div id="pfp-error" style="display: none;">
						<?php esc_html_e( 'Error!', 'pfp' ); ?> <span>&#9888;</span>
						<a class="button button-primary"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>">
							<?php esc_html_e( 'Try Again', 'pfp' ); ?>
						</a>
					</div>
				</div>
			</div>

			<div id="pfp-authorize" class="pfp-box <?php echo $tokens_available ? 'pfp-box-close' : ''; ?>">
				<div class="pfp-box-head">
					<h2><span>&#129171;</span> <?php esc_html_e( 'Get Started', 'pfp' ); ?></h2>
				</div>
				<div class="pfp-box-body">
					<a id="pfp-connect-with-bitbucket-button"
					   href="<?php echo esc_attr( Plugins_From_Pipelines::authorize_url() ); ?>"
					   class="button button-primary">

						<?php esc_html_e( 'Connect With BitBucket', 'pfp' ); ?>
					</a>
				</div>
			</div>
		</div>
		<?php
	}
}
