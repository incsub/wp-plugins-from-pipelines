<?php
/**
 * Plugin Name: Plugins From Pipelines
 * Plugin URI: https://premium.wpmudev.org
 * Description: Directly deploy plugin versions from the downloads section in Bitbucket
 * Version: 1.5.0
 * Text Domain: pfp
 * Author: Hassan Akhtar
 * Author https://premium.wpmudev.org
 */

class Plugins_From_Pipelines {
	const DEBUG = false;

	const AUTHORIZE_URL = 'https://bitbucket.org/site/oauth2/access_token';
	const DOWNLOADS_URL = 'https://api.bitbucket.org/2.0/repositories/incsub/%s/downloads';
	const ACCESS_TOKEN_OPTION_ID = 'pfp_bitbucket_access_token';
	const REFRESH_TOKEN_OPTION_ID = 'pfp_bitbucket_refresh_token';
	const ONBOARDING_DONE_OPTION_ID = 'pfp_onboarding_done';
	const AUTHORIZATION_NONCE = 'pfp_authorization';
	const CLIENT_ID = 'B7RNQ3tA2Dn6wTrunK';
	const CLIENT_SECRET = 'V2aUE9kBJrg5NtMxdMbTVHKZXdrg8gH7';

	function __construct() {
		$this->register_ajax_action( 'pfp_get_access_token', array( $this, 'json_get_access_token' ) );
		$this->register_ajax_action( 'pfp_refresh_access_token', array( $this, 'json_refresh_access_token' ) );
		$this->register_ajax_action( 'pfp_get_download_list', array( $this, 'json_get_downloads_list' ) );
		$this->register_ajax_action( 'pfp_install_plugin', array( $this, 'json_install_plugin' ) );
		$this->register_ajax_action( 'pfp_onboarding_done', array( $this, 'json_onboarding_done' ) );
	}

	public static function get_access_token() {
		return get_option( Plugins_From_Pipelines::ACCESS_TOKEN_OPTION_ID, '' );
	}

	public static function get_refresh_token() {
		return get_option( Plugins_From_Pipelines::REFRESH_TOKEN_OPTION_ID, '' );
	}

	public static function tokens_available() {
		return self::get_access_token() && self::get_refresh_token();
	}

	public static function authorize_url() {
		$api_authorize_url = sprintf( 'https://bitbucket.org/site/oauth2/authorize?client_id=%s&response_type=code', self::CLIENT_ID );
		$state_url = admin_url( 'admin-ajax.php?action=pfp_get_access_token&nonce=' . wp_create_nonce( self::AUTHORIZATION_NONCE ) );

		return add_query_arg( array( 'state' => urlencode( $state_url ) ), $api_authorize_url );
	}

	public function json_get_access_token() {
		$code = sanitize_text_field( $this->array_val( $_GET, 'auth_code' ) );
		$nonce = sanitize_text_field( $this->array_val( $_GET, 'nonce' ) );
		if ( ! $code || ! $nonce || ! wp_verify_nonce( $nonce, self::AUTHORIZATION_NONCE ) ) {
			die( 'Invalid request!' );
		}

		$response = wp_remote_post(
			self::AUTHORIZE_URL,
			array(
				'body' => array(
					'grant_type'    => 'authorization_code',
					'client_id'     => self::CLIENT_ID,
					'client_secret' => self::CLIENT_SECRET,
					'code'          => $code,
				),
			)
		);
		if ( is_wp_error( $response ) ) {
			$this->send_wp_error_response( $response );
			return;
		}

		$response_body = json_decode( wp_remote_retrieve_body( $response ), true );
		if ( ! isset( $response_body['access_token'] ) || ! isset( $response_body['refresh_token'] ) ) {
			$this->send_bitbucket_error_response( $response );
			return;
		}

		update_option( self::ACCESS_TOKEN_OPTION_ID, $response_body['access_token'] );
		update_option( self::REFRESH_TOKEN_OPTION_ID, $response_body['refresh_token'] );

		wp_safe_redirect( admin_url( 'admin.php?page=pfp_admin_page' ) );
		die();
	}

	public function json_refresh_access_token() {
		$this->check_ajax_referer();

		$response = wp_remote_post(
			self::AUTHORIZE_URL,
			array(
				'body' => array(
					'grant_type'    => 'refresh_token',
					'client_id'     => self::CLIENT_ID,
					'client_secret' => self::CLIENT_SECRET,
					'refresh_token' => self::get_refresh_token(),
				),
			)
		);
		if ( is_wp_error( $response ) ) {
			$this->send_wp_error_response( $response );
			return;
		}

		$response_body = json_decode( wp_remote_retrieve_body( $response ), true );
		if ( ! isset( $response_body['access_token'] ) ) {
			$this->send_bitbucket_error_response( $response );
			return;
		}

		update_option( self::ACCESS_TOKEN_OPTION_ID, $response_body['access_token'] );

		wp_send_json_success();
	}

	public function json_get_downloads_list() {
		$this->check_ajax_referer();

		if ( ! isset( $_POST['plugin'] ) ) {
			$this->send_json_error( esc_html__( 'Missing parameter "plugin".', 'pfp' ) );
			return;
		}

		$plugin = $_POST['plugin'];
		$response = wp_remote_get(
			sprintf( self::DOWNLOADS_URL, $plugin ),
			array(
				'headers' => array(
					'Authorization' => 'Bearer ' . self::get_access_token(),
				),
			)
		);
		if ( is_wp_error( $response ) ) {
			$this->send_wp_error_response( $response );
			return;
		}

		$response_body = json_decode( wp_remote_retrieve_body( $response ), true );
		if ( ! isset( $response_body['values'] ) ) {
			$this->send_bitbucket_error_response( $response );
			return;
		}

		ob_start();
		?>
		<label>
			<select id="build">
				<?php foreach ( $response_body['values'] as $value ) {
					$download_name = $this->array_val( $value, 'name' );
					$download_link = $this->array_val( $value, [ 'links', 'self', 'href' ] );
					?>

					<option value="<?php echo esc_attr( $download_link ); ?>">
						<?php echo esc_html( $download_name ); ?>
					</option>
				<?php } ?>
			</select>
		</label>
		<?php

		wp_send_json_success( ob_get_clean() );
	}

	public function json_install_plugin() {
		$this->check_ajax_referer();

		if ( ! isset( $_POST['url'] ) ) {
			$this->send_json_error( esc_html__( 'Missing parameter "url".', 'pfp' ) );
			return;
		}

		$installed = $this->install_plugin( $_POST['url'] );

		// Flush cache
		wp_cache_flush();

		if ( is_wp_error( $installed ) ) {
			$this->send_wp_error_response( $installed );
		} else {
			wp_send_json_success();
		}
	}

	public function json_onboarding_done() {
		update_option( self::ONBOARDING_DONE_OPTION_ID, 1 );

		wp_send_json_success();
	}

	public function add_headers( $parsed_args, $url ) {
		if ( strpos( $url, 'bitbucket' ) !== false ) {
			$parsed_args['headers'] = array(
				'Authorization' => 'Bearer ' . self::get_access_token(),
			);
		}

		return $parsed_args;
	}

	/**
	 * The bitbucket downloads URL is redirected to an AWS URL, if a request is made to the AWS URL with the
	 * bitbucket auth header, AWS complains about duplicate auth attempts. So we need to remove the auth header
	 *
	 * @param $location
	 * @param $headers
	 * @param $data
	 * @param $options
	 * @param $return
	 */
	public function remove_auth_header_to_avoid_duplicate_auth( $location, &$headers, $data, $options, $return ) {
		unset( $headers['Authorization'] );
	}

	private function install_plugin( $url ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
		require_once( ABSPATH . 'wp-admin/includes/misc.php' );
		require_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

		$remove_auth_callback = array( $this, 'remove_auth_header_to_avoid_duplicate_auth' );
		$add_header_callback = array( $this, 'add_headers' );
		$change_upgrader_options = array( $this, 'upgrader_options' );

		add_action( 'http_request_args', $add_header_callback, 10, 2 );
		add_action( 'requests-requests.before_redirect', $remove_auth_callback, 10, 5 );
		add_action( 'upgrader_package_options', $change_upgrader_options );

		$upgrader = new Plugin_Upgrader( new WP_Ajax_Upgrader_Skin() );
		$install = $upgrader->install( $url );

		remove_action( 'http_request_args', $add_header_callback );
		remove_action( 'requests-requests.before_redirect', $remove_auth_callback );
		remove_action( 'upgrader_package_options', $change_upgrader_options );

		if ( ! $install || ! $upgrader->skin->result ) {
			$errors = implode( "\n", $upgrader->skin->get_upgrade_messages() );
			return new WP_Error( 'upgrade-messages', $errors );
		}

		return $install;
	}

	private function check_ajax_referer() {
		if ( ! self::DEBUG ) {
			check_ajax_referer( 'pfp_ajax_call' );
		}
	}

	private function register_ajax_action( $action, $callback ) {
		add_action( "wp_ajax_{$action}", $callback );
		if ( self::DEBUG ) {
			add_action( "wp_ajax_nopriv_{$action}", $callback );
		}
	}

	private function array_val( $array, $key ) {
		if ( ! is_array( $key ) ) {
			$key = array( $key );
		}

		if ( ! is_array( $array ) ) {
			return null;
		}

		$value = $array;
		foreach ( $key as $key_part ) {
			$value = isset( $value[ $key_part ] ) ? $value[ $key_part ] : null;
		}

		return $value;
	}

	/**
	 * @param $error WP_Error
	 */
	private function send_wp_error_response( $error ) {
		$this->send_json_error( implode( "\n", $error->get_error_messages() ) );
	}

	private function send_bitbucket_error_response( $response ) {
		$response_body = wp_remote_retrieve_body( $response );
		$response_json = json_decode( $response_body, true );

		$error_description = $this->array_val( $response_json, 'error_description' );
		if ( ! empty( $error_description ) ) {
			$this->send_json_error( $error_description );
			return;
		}

		$error_message = $this->array_val( $response_json, array( 'error', 'message' ) );
		if ( ! empty( $error_message ) ) {
			$this->send_json_error( $error_message );
			return;
		}

		$code = wp_remote_retrieve_response_code( $response );
		if ( $code !== 200 && empty( $response_json ) && ! empty( $response_body ) ) {
			$this->send_json_error( $response_body );
			return;
		}

		$this->send_json_error(
			esc_html__( 'Unexpected response from Bitbucket. Please check your access level.', 'pfp' )
		);
	}

	private function send_json_error( $error_message ) {
		wp_send_json_error( array( 'errors' => $error_message ) );
	}

	public function upgrader_options( $options ) {
		$options['abort_if_destination_exists'] = false;
		$options['clear_destination'] = true;

		return $options;
	}
}

include_once 'class-pfp-admin-page.php';

new Plugins_From_Pipelines();
new PFP_Admin_Page();
